const fs = require('fs')
const path = require('path')

let LANGS = [
    'en',
    'ru'
]

let walkSync = function (dir, filelist) {
    let files = fs.readdirSync(dir)

    filelist = filelist || []

    files.forEach(function (file) {
        if (fs.statSync(dir + file).isDirectory()) {
            filelist = walkSync(dir + file + '/', filelist)
        } else {
            filelist.push(path.normalize(dir + '/' + file))
        }
    })

    return filelist
}

let files = walkSync(path.normalize('src/renderer/components/')),
    translates = []

for (let file of files) {
    let text = fs.readFileSync(file).toString(),
        find = text.match(/\$t\('(.*)'\)/g)

    if (find) {
        for (let i of find) {
            i = i.replace(/^\$t\('/ig, '')
            i = i.replace(/'\)$/ig, '')

            translates.push(i)
        }
    }
}

for (let lang of LANGS) {
    let filePath = path.normalize('src/renderer/i18n/' + lang + '.json'),
        langObject = {}

    try {
        let file = fs.readFileSync(filePath)

        langObject = JSON.parse(file.toString())
    } catch (e) {
        langObject = {}
    }

    for (let translate of translates) {
        if (!langObject[translate]) {
            langObject[translate] = translate
        }
    }

    fs.writeFileSync(filePath, JSON.stringify(langObject, null, 4))
}
