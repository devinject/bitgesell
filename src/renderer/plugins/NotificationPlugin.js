const NotificationPlugin = {
    install(Vue, options) {
        let vm = new Vue({
            data: {
                notifications: []
            }
        })

        Vue.prototype.$addNotification = function (text, system = false, title = '') {
            if (system) {
                let notification = new Notification(title, {
                    body: text
                })

                return;
            }

            vm.notifications.push(text)

            let count = vm.notifications.length

            if (count > 4) {
                for (let i in vm.notifications) {
                    if (i <= count - 4) {
                        vm.notifications.splice(i, 1)
                    }
                }
            }

            setTimeout(function () {
                vm.notifications.splice(0, 1)
            }, 6000)
        }

        Vue.prototype.$removeNotification = function (key) {
            vm.notifications.splice(key, 1)
        }

        Object.defineProperties(Vue.prototype, {
            "$notifications": {
                "get": function () {
                    return vm.notifications
                },

                "set": function (value) {
                    vm.notifications = value

                    return this
                }
            }
        })
    }
}

export default NotificationPlugin;
