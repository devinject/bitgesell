import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            component: require('@/components/Setup/Description').default
        },

        {
            path: '/setup/wallet',
            component: require('@/components/Setup/Wallet').default
        },

        {
            path: '/setup/access-to-wallet',
            component: require('@/components/Setup/AccessToWallet').default
        },

        {
            path: '/setup/keystore',
            component: require('@/components/Setup/Keystore').default
        },

        {
            path: '/setup/generate-seed',
            component: require('@/components/Setup/GenerateSeed').default
        },

        {
            path: '/setup/extension-seed',
            component: require('@/components/Setup/ExtensionSeed').default
        },

        {
            path: '/setup/insert-seed',
            component: require('@/components/Setup/InsertSeed').default
        },

        {
            path: '/setup/password',
            component: require('@/components/Setup/Password').default
        },

        ///////////////

        {
            path: '/main',
            component: require('@/components/Main/History').default,
        },

        {
            path: '/main/send',
            component: require('@/components/Main/Send').default,
        },

        {
            path: '/main/receive',
            component: require('@/components/Main/Receive').default,
        },

        {
            path: '/main/address',
            component: require('@/components/Main/Address').default,
        },

        {
            path: '/main/change-password',
            component: require('@/components/Main/ChangePassword').default,
        },

        {
            path: '/main/get-seed',
            component: require('@/components/Main/GetSeed').default,
        },

        ///////////////

        {
            path: '*',
            redirect: '/'
        }
    ]
})
