import axios from 'axios'

import {InsufficientFounds, InvalidSendAddress} from './Errors'
import {SERVER_URL} from '../configs'

export const SATOSHI = 100000000

export default {
    validateAddress(address) {
        if (typeof address !== 'string' || !address.length) {
            return false
        }

        if (address.length !== 43) {
            return false
        }

        if (!address.match(/^bgl1.*/)) {
            return false
        }

        return true
    },

    getMaxAmount(utxo, vByteFee, $store) {
        vByteFee = Number(vByteFee)

        let collected = utxo,
            newTx = new Transaction(),

            collectedSum = 0

        for (let i of collected) {
            newTx.addInput({
                txId: i.txid,
                vOut: i.number,
                amount: Number((i.amount * SATOSHI).toFixed(0)),
            })

            let privateKeys = this.getPrivateKeys($store, collected),

                temp = {
                    scriptPubKey: i.scriptPubKey.hex,
                    privateKey: privateKeys[i.address],
                    sigHashType: SIGHASH_ALL,
                    value: Number((i.amount * SATOSHI).toFixed(0))
                }

            newTx.signInput(collected.indexOf(i), temp)

            collectedSum += i.amount
        }

        newTx.addOutput({
            address: collected[0].address,
            value: collectedSum
        })

        newTx.commit()

        return this.roundTo8(collectedSum - (newTx.vSize * vByteFee / SATOSHI))
    },

    getPrivateKeys($store, utxo) {
        let addresses = $store.getters['Wallet/addresses'].filter(
            x => utxo.find(
                y => y.address === x.address
            )
        )

        let privateKeys = {}

        for (let i of addresses) {
            privateKeys[i.address] = i.privateKey
        }

        return privateKeys
    },

    getUtxoForCommission(address, amount, vByteFee, $store, forShow = false) {
        amount = Number(amount)
        vByteFee = Number(vByteFee)

        let utxo = $store.getters['Balance/utxo']

        if (!utxo.length) {
            throw new InsufficientFounds()
        }

        let minFee = $store.state.Balance.minFee / SATOSHI,

            isSurrender = false,
            fee = minFee,
            collected

        while (true) {
            let collectedSum = 0

            try {
                collected = this.getMinUtxo(utxo, amount + fee)
            } catch (e) {
                if (forShow) {
                    break
                } else {
                    throw e
                }
            }


            if (!collected) {

                if (forShow) {
                    break
                } else {
                    throw new InsufficientFounds()
                }
            }

            let newTx = new Transaction()

            for (let i of collected) {
                newTx.addInput({
                    txId: i.txid,
                    vOut: i.number,
                    amount: Number((i.amount * SATOSHI).toFixed(0)),
                })

                let privateKeys = this.getPrivateKeys($store, collected),
                    temp = {
                        scriptPubKey: i.scriptPubKey.hex,
                        privateKey: privateKeys[i.address],
                        sigHashType: SIGHASH_ALL,
                        value: Number((i.amount * SATOSHI).toFixed(0))
                    }

                newTx.signInput(collected.indexOf(i), temp)

                collectedSum += i.amount
            }

            isSurrender = collectedSum > (amount + fee)

            if (amount === this.getMaxAmount($store.getters['Balance/utxo'], vByteFee, $store)) {
                isSurrender = false
            }

            try {
                if (isSurrender) {
                    newTx.addOutput({
                        address: address,
                        value: amount
                    })

                    newTx.addOutput({
                        address: collected[0].address,
                        value: this.roundTo8(collectedSum - amount - fee)
                    })
                } else {
                    newTx.addOutput({
                        address: address,
                        value: this.roundTo8(amount + fee)
                    })
                }
            } catch (e) {
                throw new InvalidSendAddress()
            }

            newTx.commit()

            fee = Number((newTx.vSize * vByteFee / SATOSHI).toFixed(8))

            if (this.roundTo8(collectedSum - amount - fee) >= 0) {
                isSurrender = Number((collectedSum - amount - fee).toFixed(8)) !== 0

                break
            }
        }

        if (fee < minFee) {
            fee = minFee
        }

        return {
            fee: fee,
            utxo: collected,
            isSurrender: isSurrender
        }
    },

    async send(address, amount, changeTitle, fee, utxo, vByteFee, $store, changeAddress = '') {
        amount = Number(amount)
        fee = fee / SATOSHI

        // if (amount === this.getMaxAmount($store.getters['Balance/utxo'], vByteFee, $store)) {
        // 	fee /= 2
        // }

        let sum = amount + fee,
            privateKeys = this.getPrivateKeys($store, utxo),
            vins = this.getMinUtxo(utxo, sum)

        if (!changeAddress || !changeAddress.length) {
            changeAddress = vins[0].address
        }

        if (address === changeAddress) {
            changeAddress = await $store.dispatch('Wallet/unusedAddress', changeTitle)
        }

        await this.sendTransaction(vins, sum, fee, address, changeAddress, privateKeys)
    },

    getMinUtxo(utxo, sum) {
        utxo = utxo.sort((a, b) => a.amount > b.amount ? 1 : 0)

        // TODO: read about important things for min UTXO
        let currentSum = 0,
            answer = []

        for (let i of utxo) {
            currentSum += i.amount
            answer.push(i)

            if (currentSum >= sum) {
                return answer
            }
        }

        throw new InsufficientFounds()
    },

    roundTo8(number) {
        return Number((number).toFixed(8))
    },

    async sendTransaction(vouts, sum, fee, addressString, saveAddress, privateKeys) {
        let vin = [],
            newSum = 0

        fee = fee * SATOSHI

        for (let i of vouts) {
            vin.push({
                txid: i.txid,
                vout: i.number
            })

            newSum += i.amount
        }

        let useSave = newSum - fee > sum,

            newOuts = [],
            voout = {}

        // if (useSave) {
        voout[addressString] = this.roundTo8(sum)
        // } else {
        // 	voout[addressString] = this.roundTo8(sum - fee)
        // }

        newOuts.push(voout)

        if (useSave) {
            let voout = {}

            voout[saveAddress] = this.roundTo8(newSum - fee - sum)
            newOuts.push(voout)
        }

        let rawTx = (await axios.post(SERVER_URL + 'site/createrawtransaction', {props: [vin, newOuts]})).data,
            tx = new Transaction({rawTx})

        for (let i in vouts) {
            let temp = {
                scriptPubKey: vouts[i].scriptPubKey.hex,
                privateKey: privateKeys[vouts[i].address],
                sigHashType: SIGHASH_ALL,
                value: Number((vouts[i].amount * SATOSHI).toFixed(0))
            }

            tx.signInput(i, temp)
        }

        return (await axios.post(SERVER_URL + 'site/sendrawtransaction', {
            txid: tx.txId,
            props: [tx.serialize()]
        })).data
    }
}
