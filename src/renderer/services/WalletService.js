const fs = require('fs')
const path = require('path')

import CryptoJS from 'crypto-js'
import {ipcRenderer} from 'electron'
import {ReadWallet} from './Errors'

const rightPath = ipcRenderer.sendSync('get-path')

const ENCRYPT_WORD = 'devinject'

export default {
    getLocalWallets() {
        let files = fs.readdirSync(rightPath + '/'),
            wallets = []

        for (let i of files) {
            if (i.match(/\.di$/)) {
                wallets.push(i.replace('.di', ''))
            }
        }

        return wallets
    },

    validateWallet(walletData) {
        if (!walletData.mnemonic || !walletData.passphrase || !walletData.addressesCount || !walletData.addressesLabels) {
            throw new ReadWallet()
        }
    },

    openLocalWallet(name) {
        let file = path.resolve(rightPath + '/', (name + '.di'))

        return this.readWallet(file)
    },

    createFilePath(walletName) {
        return path.resolve(rightPath + '/', (walletName + '.di'))
    },

    openRemoteWallet(filePath) {
        return this.readWallet(filePath)
    },

    buildWalletFromStore(store) {
        return CryptoJS.AES.encrypt(JSON.stringify({
            mnemonic: store.state.Wallet.seed,
            passphrase: store.state.Wallet.passphraseEncrypt,
            addressesCount: store.state.Wallet.addressesCount,
            addressesLabels: store.state.Wallet.addressesLabels,
        }), ENCRYPT_WORD).toString()
    },

    readToStoreFromWalletData(store, jsonWallet) {
        store.commit('Wallet/seed', jsonWallet.mnemonic)
        store.commit('Wallet/passphraseEncrypt', jsonWallet.passphrase)
        store.commit('Wallet/addressesCount', jsonWallet.addressesCount)
        store.commit('Wallet/addressesLabels', jsonWallet.addressesLabels)
    },

    dumpWallet(mnemonicEncrypt, passphraseEncrypt) {
        return {
            seed: mnemonicEncrypt,
            phrase: passphraseEncrypt
        }
    },

    encrypt(mnemonic, passphrase, password) {
        return {
            seed: CryptoJS.AES.encrypt(mnemonic, password).toString(),
            passphraseEncrypt: CryptoJS.AES.encrypt(passphrase, password).toString()
        }
    },

    decrypt(seed, passphraseEncrypt, password) {
        return {
            mnemonic: CryptoJS.AES.decrypt(seed, password).toString(CryptoJS.enc.Utf8),
            passphrase: CryptoJS.AES.decrypt(passphraseEncrypt, password).toString(CryptoJS.enc.Utf8)
        }
    },

    validateSeed(words) {
        return window.isMnemonicCheckSumValid(words) && window.isMnemonicValid(words)
    },

    /////////////////

    readWallet(filePath) {
        let file = fs.readFileSync(filePath),
            decrypted = CryptoJS.AES.decrypt(file.toString(), ENCRYPT_WORD).toString(CryptoJS.enc.Utf8),
            walletData = JSON.parse(decrypted)


        this.validateWallet(walletData)

        walletData.filePath = filePath

        return walletData
    },

    saveWallet(store) {
        let json = this.buildWalletFromStore(store)

        fs.writeFileSync(store.state.Wallet.filePath, json)
    },

    download(store) {
        let json = this.buildWalletFromStore(store)

        let blob = new Blob([json], {type: 'text/plain'}),
            url = window.URL.createObjectURL(blob),
            a = document.createElement('a')

        document.body.appendChild(a)

        let name = store.state.Wallet.walletName ? store.state.Wallet.walletName : 'wallet'

        a.href = url
        a.download = name + '.di'
        a.style = 'display: none'

        a.click()

        window.URL.revokeObjectURL(url)
        document.body.removeChild(a)
    }
}
