import axios from 'axios'

import {SERVER_URL} from '../configs'

export default {
    async getTransactions(allAddresses) {
        let addresses = allAddresses.map(x => x.address)

        return (await axios.post(SERVER_URL + 'site/addresses', {
            addresses
        })).data
    }
}
