import {app, BrowserWindow, Menu, globalShortcut, ipcMain, powerMonitor} from 'electron'

import path from 'path'

//////////////////////////////

if (process.env.NODE_ENV !== 'development') {
    global.__static = path.join(__dirname, '/static').replace(/\\/g, '\\\\')
}

const winURL = process.env.NODE_ENV === 'development' ? `http://localhost:9080` : `file://${__dirname}/index.html`;

//////////////////////////////

let win

function createWindow() {
    win = new BrowserWindow({
        title: 'BGL Wallet',
        backgroundColor: '#262626',

        minWidth: 1024,
        minHeight: 607,
        width: 1024,
        height: 607,

        resizable: true,
        frame: true,
        center: true,
        autoHideMenuBar: true,

        webPreferences: {
            webSecurity: false,
            nodeIntegration: true,
            nodeIntegrationInWorker: false,
            spellcheck: false
        }
    })

    win.loadURL(winURL)

    win.on('closed', () => {
        win = null
    })
}

//////////////////////////////

app.on('ready', () => {
    createWindow()

    Menu.setApplicationMenu(Menu.buildFromTemplate([{
        label: 'Edit',
        submenu: [
            {label: 'Undo', accelerator: 'CmdOrCtrl+Z', selector: 'undo:'},
            {label: 'Redo', accelerator: 'Shift+CmdOrCtrl+Z', selector: 'redo:'},
            {type: 'separator'},
            {label: 'Cut', accelerator: 'CmdOrCtrl+X', selector: 'cut:'},
            {label: 'Copy', accelerator: 'CmdOrCtrl+C', selector: 'copy:'},
            {label: 'Paste', accelerator: 'CmdOrCtrl+V', selector: 'paste:'},
            {label: 'Select All', accelerator: 'CmdOrCtrl+A', selector: 'selectAll:'}
        ]
    }]))

    ipcMain.on('window-close', () => {
        win.close()
    })

    ipcMain.on('window-minimize', () => {
        win.minimize()
    })

    ipcMain.on('window-fullscreen', () => {
        win.setFullScreen(win.isFullScreen())
    })

    ipcMain.on('idle-time', (event, arg) => {
        event.returnValue = powerMonitor.getSystemIdleTime()
    })

    ipcMain.on('get-path', (event, arg) => {
        event.returnValue = process.env.NODE_ENV === 'development' ? './' : app.getPath('userData')
    })
})

app.on('window-all-closed', () => {
    app.quit()
})

app.on('activate', () => {
    if (win === null) {
        createWindow()
    }
})

app.on('browser-window-focus', function () {
    globalShortcut.register('CommandOrControl+R', () => {
        console.log('CommandOrControl+R is pressed: Shortcut Disabled')
    })

    globalShortcut.register('F5', () => {
        console.log('F5 is pressed: Shortcut Disabled')
    })
})

app.on('browser-window-blur', function () {
    globalShortcut.unregister('CommandOrControl+R')
    globalShortcut.unregister('F5')
})
