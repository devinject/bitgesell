'use strict'

process.env.BABEL_ENV = 'main'

const webpack = require('webpack'),
    path = require('path'),
    {dependencies} = require('../package.json'),
    MinifyPlugin = require("babel-minify-webpack-plugin")

let mainConfig = {
    entry: {
        main: path.join(__dirname, '../src/main/index.js')
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, '../dist/electron')
    },

    resolve: {
        extensions: ['.js', '.json', '.vue', '.node']
    },
    externals: [
        ...Object.keys(dependencies || {})
    ],

    node: {
        __dirname: process.env.NODE_ENV !== 'production',
        __filename: process.env.NODE_ENV !== 'production'
    },
    target: 'electron-main',

    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.node$/,
                use: 'node-loader'
            }
        ]
    },

    plugins: [
        new webpack.NoEmitOnErrorsPlugin()
    ]
}

if (process.env.NODE_ENV !== 'production') {
    mainConfig.plugins.push(
        new webpack.DefinePlugin({
            '__static': `"${path.join(__dirname, '../static').replace(/\\/g, '\\\\')}"`
        })
    )
}


if (process.env.NODE_ENV === 'production') {
    mainConfig.plugins.push(
        new MinifyPlugin(),

        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"'
        })
    )
}

module.exports = mainConfig
